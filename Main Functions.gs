// Google Drive Copy Scripts
// Vancouver Community Network
// Aug 8, 2018
// Version 1.0

// MAIN FUNCTION SET.
// Intended for system to be called from here.

// PURPOSE: To duplicate Google Drive file trees from one Drive to another, and verify the transfer.


// CONFIG:
//File IDs are found at the end of the URL when browsing the folder in Google Drive.
var inFolderId = "";
var outFolderId = "";


//Copies the file tree from the 'inDriveId' folder to the 'outDriveId' folder.
// Logs statistics, DNE files or folders.
// Does not check for changes in the file (Yet, TODO)
// Does not overwrite. Cannot not copy complex/looping/multi-parent hierarchies.
function driveCopy() {
  driveCopyInitiator(inFolderId,outFolderId);
}

//Steps through the file tree on either side, comparing file lengths of those with matching names.
// Logs everything: Mismatch, match, DNE's, etc.
//NOTE: As it requires everything to be pulled as a blob, then converted to a byteArray, takes -quite- a bit of time.
// May throw 'Server Error Occurred' or timeout errors. Recommend using it on smaller folder trees.
function compareFiles() {
  compareFileSizes(inFolderId, outFolderId);
}

//Logs a line with an integer, counting the number of files in the inFolderId file tree.
function countFilesInFolder() {
  countFiles(inFolderId);
}

//Logs a line with an integer, counting the number of files in the outFolderId file tree.
function countFilesOutFolder() {
  countFiles(outFolderId);
}

//Pushes to the Logger any duplicate files found in the Tree.
//Would cause issues with the driveCopy() functionality if there are multiple files in a folder with the same name.
function findDuplicateFileNamesInFolder() {
  findDuplicateNames(inFolderId);
  Logger.log("FindDuplicate function complete.");
}

function findDuplicateFileNamesOutFolder() {
  findDuplicateNames(outFolderId);
  Logger.log("FindDuplicate function complete.");
}

